using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public GameObject loseTextObject;

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;

    public Timer time;
  
    // Start is called before the first frame update

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        //Set the count to zero
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);

        SetCountText();
        loseTextObject.SetActive(false);
    }

    
    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 12 && time.timeRemaining != 0.0) 
        {
            winTextObject.SetActive(true);
        }
        
        countText.text = "Count: " + count.ToString();
        if(count <= 12 && time.timeRemaining <= 0.0)
        {
            loseTextObject.SetActive(true);
        }
    }

    void FixedUpdate()
    {
        
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");
 
        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
 
       // rb.AddForce (movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            SetCountText();
        }
    }

}