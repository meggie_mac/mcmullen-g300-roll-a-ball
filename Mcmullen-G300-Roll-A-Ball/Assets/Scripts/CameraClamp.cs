using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraClamp : MonoBehaviour
{
   [SerializeField]
   private Transform targetToFollow;

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(
            Mathf.Clamp(targetToFollow.position.x, -3.5f, 3.5f),
            Mathf.Clamp(targetToFollow.position.y, -3f, 3f),
            transform.position.z);
        
    }

    //Script from Alexander Zotov
}


